# Hooking up anything to twitter. Put the credentials like keys and secret in a .json file meeting the requirements.
# I hope this one is not too crappy.
# Author: Jakob Ledig

require 'rubygems'
require 'oauth'
require 'json'
require 'logger'

class TwitterAPI
  def initialize
    @logger = Logger.new(STDOUT)
    consumer_key = JSON.parse(IO.read('twitter_oauth.json'))['consumer_key']
    consumer_key_secret = JSON.parse(IO.read('twitter_oauth.json'))['consumer_secret']
    @consumer_key = OAuth::Consumer.new(consumer_key, consumer_key_secret)
    access_token = JSON.parse(IO.read('twitter_oauth.json'))['access_token']
    access_token_secret = JSON.parse(IO.read('twitter_oauth.json'))['access_token_secret']
    @access_token = OAuth::Token.new(access_token, access_token_secret)
  end

  def check_credentials
    address = URI('https://api.twitter.com/1.1/account/verify_credentials.json')
    http = Net::HTTP.new address.host, address.port
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Get.new address.request_uri
    request.oauth! http, @consumer_key, @access_token
    http.start
    http.request request
  end

  def tweet(text)
    address = URI('https://api.twitter.com/1.1/statuses/update.json')
    http = Net::HTTP.new address.host, address.port
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    request = Net::HTTP::Post.new address.request_uri
    request.set_form_data('status' => text.to_s)
    request.oauth! http, @consumer_key, @access_token
    http.start
    http.request request
  end
end
