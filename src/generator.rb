# Simple random generator, hooking up words to sometimes authentical True Metal Songtitles.
# Returns a new song title if "new_song" is called (surprise!), the structure of which
# can be enforced as the numbers indicate.
# Author: Jakob Ledig

class Generator
  def initialize
    @prefixes = IO.read('lists/prefixes.txt').lines
    @prefixes.each(&:chomp!)
    @nouns = IO.read('lists/nouns.txt').lines
    @nouns.each(&:chomp!)
    @nouns += build_plurals
    @suffixes = IO.read('lists/suffixes.txt').lines
    @suffixes.each(&:chomp!)
    @verbs = IO.read('lists/verbs.txt').lines
    @verbs.each(&:chomp!)
  end

  def new_song(structure = rand(1..7))
    initialize
    add_in = ->(text) {text + ' ' if rand(2) == 1}
    case structure
      when 0
        @nouns.sample.to_s
      when 1
        "#{@prefixes.sample.capitalize} #{new_song(0)}"
      when 2
        "#{new_song(1)} #{@suffixes.sample}"
      when 3
        "#{@verbs.sample.capitalize} #{add_in.call('the')}#{new_song(1)}"
      when 4
        "#{@verbs.sample.capitalize} #{add_in.call('the')}#{new_song(2)}"
      when 5
        "#{@prefixes.sample.capitalize}ly #{new_song(4)}"
      when 6
        "#{new_song(2)} #{new_song(3)}"
      when 7
        "#{add_in.call('The')}#{new_song(0)}\'s #{new_song(rand(0..2))}"
      else
        ''
    end
  end

  def build_plurals
    plurals = []
    @nouns.each do |noun|
      chars = noun.chars
      if chars.last == 'y'
        chars.delete_at(-1)
        chars += 'ies'.chars
      elsif (chars[-1] + chars[-2]) == 'ss'
        chars += 'es'.chars
      elsif chars.join == 'Man'
        chars = 'Men'.chars
      else
        chars += 's'.chars
      end
      plurals << chars.join
    end
    plurals
  end
end