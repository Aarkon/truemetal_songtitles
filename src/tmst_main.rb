# RUN THIS FILE IF YOU WANT TO USE THE SOFTWARE
# Main script of tue True Metal Songtitle Generator.
# Author: Jakob Ledig

require_relative 'generator'
require_relative 'twitter_api'
require 'logger'

VERSION = 1.3

@generator = Generator.new
@twitter = TwitterAPI.new

def main_loop(logger, internal_clock)
  song_title = @generator.new_song
  logger.info("Entering main loop with #{song_title} as a song title")
  response = @twitter.tweet(song_title)
  if response.code == '200'
    logger.info("Successfully tweeted #{JSON.parse(response.body)['text']}!")
    sleep 3600 * 3 if internal_clock
  else
    sleep 10
    logger.info(@twitter.tweet(song_title))
  end
  main_loop(logger, internal_clock) if internal_clock
end

# Calling the main loop initially
logger = Logger.new(STDOUT)
main_loop(logger, true)
