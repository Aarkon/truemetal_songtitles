FROM ruby

COPY src src/

RUN gem install oauth

WORKDIR src

CMD ["ruby", "tmst_main.rb"]
