# True Metal Songtitle Generator

## Dafuq?
A random generator for True Metal songtitles attatched to a crappy implementation of the Twitter API. Now dockerized.

Can be used for any kind of randomly generated content. In that case, just play around with lists of terms that the generator-class reads.

Results can be admired here:  

https://twitter.com/TrueMetalSongs  
https://www.facebook.com/TrueMetalSongtitles/  

## Set up
- clone the repo
- put your twitter api secrets in a json formatted textfile `src/twitter_oauth.json`, looking like this:
```{
 	"consumer_key": "$your consumer key",
 	"consumer_secret": "$your consumer secret",
 	"access_token": "$your access token",
 	"access_token_secret": "$your access token secret"
 }
```
- from the projects top level directory, run `docker build -t truemetal_songtitles .` (replace `truemtal_songtitles` with whatever you like, but don't omit the dot at the end)
- run `docker run truemetal_songtitles` 

## Development

Be sure to call the tmst_main.rb file from within the src-directory. Using RubyMine, you can right click the directory and mark it as `Load Path Route`.
 